## 0.5.3 (2024-12-24)

### Fix

- **pyproject.toml**: Hugoを更新した: 0.134.3 -> 0.140.1
- **blowfish/themes/blowfish**: テーマを更新した: v2.80.0
- **blowfish/content/about/foundation.md**: 設立趣意書を本文に書いた
- **blowfish/content/about/foundation.md**: 設立趣意書のパスを修正した
- **blowfish/content/posts/2023/20231216_fundamentalz/index.md**: ファンダメンタルズフェスを追加した
- **blowfish/content/posts/2020/20200608_fundamentalz/index.md**: 記事を追加した
- **blowfish/content/posts/2020/20200928_meetup/index.md**: 日付を修正した

## 0.5.2 (2024-12-01)

Sphinx関係のパッケージやファイルを削除した

### Fix

- **pyproject.toml**: fixed version_files
- **LICENSE**: fixed copyright
- **blowfish/static/jacst_prospectus.pdf**: ファイルを移動した
- **blowfish/static/favicon.ico**: 新しいfaviconを追加した
- **static/**: faviconなどを移動した
- **blowfish/content/posts/2024/20241220_gm19/index.md**: 第19回総会の開催案内を追加した
- **blowfish/content/posts/2024/20241129_workshop/index.md**: 開催報告に変更した
- **requirements.txt**: removed requirements.txt
- **pyproject.toml**: removed myst-parser
- **pyproject.toml**: removed sphinx
- **pyproject.toml**: removed sphinx-book-theme
- **pyproject.toml**: removed sphinx-design
- **pyproject.toml**: removed pandas
- **pyproject.toml**: removed sphinxcontrib-datatemplates
- **pyproject.toml**: removed sphinx-autobuild

### Refactor

- **docs/**: 設定ファイルを削除した
- **docs/_templates/**: データテンプレートを削除した - deleted:    docs/_templates/my-data/news-list.tmpl
- **docs/about/**: ページを削除した
- **docs/guidebook/**: ページを削除した
- **docs/join/index.md**: ページを削除した
- **docs/post/**: ページを削除した
- **docs/report.md**: 削除した
- **docs/_static/css/heading.css**: カスタムCSSを削除した
- **docs/_static/jacst.png**: ロゴを削除した

## 0.5.1 (2024-11-19)

### Fix

- **blowfish/config/blowfish/menus.ja.toml**: メニューのアイコンを変えてみた
- **blowfish/config/blowfish/menus.ja.toml**: メニューを修正した
- **blowfish/content/posts/2024/20241129_workshop/index.md**: 広報研究部会のワークショップを追加した

## 0.5.0 (2024-11-14)

SphinxからHugoに移行した

### Feat

- **hugo/content/guidebook/_index.md**: ガイドブックのセクションを作成した
- **hugo/config/blowfish/params.toml**: テーマのパラメータを設定した
- **hugo/content/join/index.md**: 入会の手引きを移行した
- **hugo/themes/blowfish**: Blowfishテーマをサブモジュールとして追加した
- **hugo/hugo.toml**: Hugo用のディレクトリを追加した

### Fix

- **blowfish/content/_index.md**: 概要の区切りを追加した
- **blowfish/config/_default/hugo.toml**: OGP画像のパスを修正した
- **blowfish/config/_default/hugo.toml**: OGP設定を追加した
- **blowfish/config/_default/hugo.toml**: added GA4 tag
- **blowfish/config/blowfish/config.toml**: fixed baseURL
- **blowfish/content/posts/2016/20161026_seminar/index.md**: 開催日を修正した
- **blowfish/content/posts/2016/20160922_nsl2016/index.md**: 公開日を修正した
- **blowfish/content/posts/2016/20170314_symposium/index.md**: moreを追加した
- **blowfish/content/posts/2016/20170314_symposium/index.md**: シンポジウム@広島大学を追加した
- **blowfish/content/posts/2016/20160922_nsl2016/index.md**: New Scientist Liveを追加した
- **blowfish/content/posts/2016/20161026_seminar/index.md**: 第4回セミナーを追加した
- **blowfish/content/posts/2016/20170219_aaas_2017/index.md**: AAAS2017を追加した
- **blowfish/content/posts/2016/20161206_media_13/index.md**: 第13回プレゼン会を追加した
- **blowfish/content/posts/2016/20160408_media_12/index.md**: 第12回プレゼン会を追加した
- **blowfish/content/posts/2016/20160428_gm_06/index.md**: 第6回総会を追加した
- **blowfish/content/posts/2016/20161026_gm_07/index.md**: 第7回総会を追加した
- **blowfish/content/posts/2016/_index.md**: 2016年度を追加した
- **blowfish/content/about/secretariat/2024.md**: 2024年期の役員を追加した
- **blowfish/content/about/_index.md**: カレンダーを埋め込んだ
- **blowfish/content/posts/2015/20150425_workshop/index.md**: ニコニコ超会議を追加した
- **blowfish/content/posts/2015/20150428_seminar/index.md**: 第1回セミナーを追加した
- **blowfish/content/posts/2015/20150529_seminar/index.md**: EAセミナーを追加した
- **blowfish/content/posts/2015/20150703_seminar/index.md**: セミナーを追加した
- **blowfish/content/posts/2015/20151204_symposium/index.md**: タグを修正した
- **blowfish/content/posts/2015/20150709_workshop/index.md**: ワークショップを追加した
- **blowfish/content/posts/2015/20150816_workshop/index.md**: ISCW2015を追加した
- **blowfish/content/posts/2015/20151204_symposium/index.md**: シンポジウムを追加した
- **blowfish/content/posts/2015/20160220_aaas_2016/index.md**: AAAS2016を追加した
- **blowfish/content/posts/2015/20151116_gm_05/index.md**: 第5回総会を追加した
- **blowfish/content/posts/2015/20150710_gm_04/index.md**: 第4回総会を追加した
- **blowfish/content/posts/2015/20150612_media_11/index.md**: 第11回プレゼン会を追加した
- **blowfish/content/posts/2015/_index.md**: 2015年度を追加した
- **blowfish/content/posts/2017/20180309_gm_09_jacst10th/index.md**: 第9回総会を追加した
- **blowfish/content/posts/2017/20170620_gm_08/index.md**: 第8回総会を追加した
- **blowfish/content/posts/2017/20170420_book/index.md**: ガイドブックを公開した
- **blowfish/content/posts/2017/20170707_hokkaido/index.md**: 北大で開催したシンポジウムを追加した
- **blowfish/content/posts/2017/20170914_media_14/index.md**: 第14回プレゼン会を追加した
- **blowfish/content/posts/2017/20180215_aaas_2018/index.md**: AAAS2018を追加した
- **blowfish/content/posts/2017/20171030_wcsj_2017/index.md**: WCSJ2017を追加した
- **blowfish/content/posts/2017/20180309_gm_09_jacst10th/index.md**: タグを追加した
- **blowfish/content/posts/2023/20230512_gm_17_jacst15th/index.md**: 15周年記念シンポジウムを整理した
- **blowfish/content/posts/2017/20180309_gm_09_jacst10th/index.md**: 10周年記念シンポジウムを追加した
- **blowfish/content/posts/2017/20170620_gm_08/index.md**: 第8回総会を追加した
- **blowfish/content/posts/2017/_index.md**: 2017年度を追加した
- **blowfish/content/posts/2014/20150319_workshop/index.md**: OISTワークショップを追加した
- **blowfish/content/posts/2014/20141110_workshop/index.md**: クローズド・ワークショップを追加した
- **blowfish/content/posts/2014/20140805_meetup_07/index.md**: 第7回勉強会を追加した
- **blowfish/content/posts/2014/20141120_media_10/index.md**: 第10回プレゼン会を追加した
- **blowfish/content/posts/2014/20141120_gm_03/index.md**: 第3回総会を追加した
- **blowfish/content/posts/2014/20140610_gm_02/index.md**: 第2回総会を追加した
- **blowfish/content/posts/2014/_index.md**: 2014年度を追加した
- **blowfish/content/posts/2007/20071226_meetup/index.md**: JACST初会合の記事を追加した
- **blowfish/content/posts/2007/20071023_agora_2007/index.md**: JACSTたちあげの記事を追加した
- **blowfish/content/posts/2007/_index.md**: 2007年度を追加した
- **blowfish/content/posts/2018/20181027_handbook/index.md**: ガイドブックのトピックスを追加した
- **blowfish/content/posts/2018/20190214_aaas_2019/index.md**: AAAS2019を追加した
- **blowfish/content/posts/2018/20181206_meetup/index.md**: 勉強会を追加した
- **blowfish/content/posts/2018/20181206_gm_10/index.md**: 第10回総会を開催しました
- **blowfish/content/posts/2018/20181206_media_16/index.md**: 第16回プレゼン会を追加した
- **blowfish/content/posts/2018/20180627_media_15/index.md**: 第15回プレゼン会を追加した
- **blowfish/content/posts/2018/_index.md**: 2018年度を追加した
- **blowfish/content/posts/2019/20191125_workshop/index.md**: cardView崩れを修正
- **blowfish/config/_default/hugo.toml**: 設定キーを修正した
- **blowfish/themes/blowfish**: テーマを更新した: v2.78.0
- **blowfish/content/_index.md**: トップページのレイアウトを変更した
- **blowfish/config/blowfish/params.toml**: トップ（最近の記事）の表示方法を変更した
- **blowfish/content/posts/_index.md**: トピックス一覧の表示を変更した
- **blowfish/content/posts/2019/20200229_kids/index.md**: 臨時休校サイトを追加した
- **blowfish/content/posts/2019/20200213_aaas_2020/index.md**: AAAS2020を追加した
- **blowfish/content/posts/2019/20191125_workshop/index.md**: Japan PIO Summitを追加した
- **blowfish/content/posts/2019/20191213_gm_12/index.md**: 第12回総会を追加した
- **blowfish/content/posts/2019/20190628_gm_11/index.md**: 第11回総会を追加した
- **blowfish/content/posts/2019/20191212_media_18/index.md**: 第18回プレゼン会を追加した
- **blowfish/content/posts/2019/20190628_media_17/index.md**: 第17回プレゼン会を追加した
- **blowfish/content/posts/2019/_index.md**: 2019年度を追加した
- **blowfish/content/posts/2020/20200928_meetup/index.md**: オンライン勉強会を追加した
- **blowfish/content/posts/2020/20210218_seminar/index.md**: プレスセミナーを追加した
- **blowfish/content/posts/2020/20210325_fundamentalz/index.md**: ファンダメンタルズトークを追加した
- **blowfish/content/posts/2020/20200930_media_19/index.md**: 第19回プレゼン会の開催報告を校正
- **blowfish/content/posts/2021/20220302_fundamentalz/index.md**: ファンダメンタルズminiを追加した
- **blowfish/content/posts/2021/20220116_guidebook/index.md**: ハンドブックを更新した
- **blowfish/content/posts/2021/20220115_guidebook/index.md**: ロジックモデル・テンプレートを公開した
- **blowfish/content/posts/2021/20210709_media_20/index.md**: 第20回プレゼン会を追加した
- **blowfish/config/blowfish/params.toml**: リスト表示でcardViewを無効にした
- **blowfish/content/posts/2022/20220902_casestudy/index.md**: オンライン勉強会を追加した
- **blowfish/content/posts/2022/20220708_media_21/index.md**: 第21回プレゼン会
- **blowfish/content/guidebook/**: 概要エリアからリンクを削除した
- **blowfish/content/posts/2020/20210226_gm_14/index.md**: 第14回総会を追加した
- **blowfish/content/posts/2020/20200930_media_19/index.md**: 第19回プレゼン会を追加した
- **blowfish/content/posts/2020/20200710_gm_13/index.md**: 第13回総会を追加した
- **blowfish/data/authors/fundamentalz.json**: 隣接領域部会を追加した
- 記事の日付を修正した
- **blowfish/content/posts/2022/20220701_gm_16/index.md**: 第16回総会を追加した
- **blowfish/content/posts/2021/20220114_gm_15/index.md**: 第15回総会を追加した
- リストページの日付を修正した
- **blowfish/content/posts/2022/20220408_mext/index.md**: 文科大臣表彰を追加した
- **blowfish/content/posts/2021/_index.md**: 2021年度のリストページを追加した
- **blowfish/content/posts/2022/_index.md**: 2022年度のリストページを追加した
- **blowfish/content/posts/2023/20230512_jacst15th/index.md**: 15周年記念シンポジウムを追加した
- **blowfish/content/posts/2023/20230511_gm_17/index.md**: 第17回総会を追加した
- **blowfish/content/posts/2023/20230929_media_22/index.md**: 第22回プレゼン会を追加した
- **blowfish/data/authors/office.json**: 事務局ページを追加した
- **blowfish/content/posts/2023/20240209_gm_18/index.md**: 第18回総会の開催記事を追加した
- **blowfish/content/posts/2023/_index.md**: 2023年度のリストページを追加した
- **blowfish/content/posts/2020/_index.md**: 2020年度のリストページを追加した
- **blowfish/content/posts/2024/_index.md**: 2024年度のリストページを作成した
- **.pre-commit-config.yaml**: hookを追加した: check-toml
- **scripts/snapd/_jacst_reports.csv**: 余白を削除した
- **.pre-commit-config.yaml**: hookを追加した: trailing-whitespace
- **blowfish/content/posts/2024/20240830_media_23rd/index.md**: 著者を追加した
- **blowfish/content/posts/2024/20240830_media_23rd/index.md**: 第23回プレゼン会の記事を追加した
- **.gitlab-ci.yml**: パスを修正した
- **blowfish/themes/blowfish**: テーマのパスを変更した
- **blowfish/**: ディレクトリ名を変更した: hugo -> blowfish
- **hugo/config/blowfish/config.toml**: baseURLを修正した
- **poetry.lock**: パッケージを更新した
- **pyproject.toml**: hugoを追加した: 0.134.2
- **hugo/content/join/index.md**: 微修正した
- **hugo/content/join/index.md**: 会員専用MLの説明を修正した
- **hugo/themes/blowfish**: テーマを更新した: v2.77.1
- **hugo/themes/blowfish**: blowfishを更新した: v2.75.0
- **hugo/config/_default/hugo.toml**: 一部の設定をデフォルト値に移動した
- **hugo/themes/blowfish**: テーマを更新した
- **hugo/content/join/index.md**: メタデータを修正した
- **hugo/content/join/index.md**: 入会の手引きに会則へのリンクを追加した
- **hugo/content/bylaws/index.ja.md**: 会則を追加した
- **hugo/config/blowfish/languages.ja.toml**: 設定を変更した
- **hugo/config/blowfish/params.toml**: cardViewの幅を変更した
- **hugo/config/blowfish/config.toml**: permalinksを追加した
- **hugo/content/posts/_index.md**: 活動報告のサンプルを追加した
- **hugo/content/posts/2020/20201001_media_19th/index.md**: タグなどを追加した
- **hugo/data/authors/kids.json**: 臨時休校対応部会を著者に追加した
- **hugo/data/authors/media.json**: 部会を追加した
- **hugo/config/blowfish/languages.ja.toml**: 著者を追加した
- **hugo/data/authors/office.json**: 事務局を追加した
- **hugo/content/posts/_index.md**: 記事のサンプルを追加した
- **.gitignore**: resourcesディレクトリを除外した
- ** .gitignore**: Go関係のファイルを除外した
- **hugo/config/blowfish/menus.ja.toml**: メニューを変更した
- **hugo/config/blowfish/params.toml**: mainSectionsを設定した
- **hugo/content/about/_index.md**: 表示内容を変更した
- **hugo/content/about/_index.md**: 機関のGoogleマップを移動した
- **hugo/content/about/_index.md**: 会員数／機関数のファイルをまとめた
- **hugo/assets/hero/featured_**: 画像のファイル名を変更した
- **hugo/assets/hero/**: デフォルトのhero画像を追加した
- **hugo/assets/logo/jacst.png**: ロゴ画像を追加した
- **hugo/content/about/data/**: CSVファイルを削除した
- **hugo/config/blowfish/menus.ja.toml**: メニュー横にアイコンを追加した
- **hugo/content/about/members.md**: 会員数を追加した
- **hugo/content/about/map.md**: 順番を調整した
- **#	new file:   hugo/content/about/secretariat/_index.md**: これまでの役員・事務局の担当者を追加した
- **hugo/content/about/foundation.md**: 設立時の情報を別ページにした
- **hugo/content/about/map.md**: 参加機関の地図を追加した
- **hugo/content/about/members.md**: 会員数を別ページにした
- **hugo/content/about/_index.md**: Page Bundleに変更した
- **hugo/content/guidebook/_index.md**: cardView = falseに設定した
- **hugo/content/guidebook/stempra/index.md**: Stempraを追加した
- **hugo/content/guidebook/logicmodel/index.md**: ロジックモデルを追加した
- **hugo/content/guidebook/handbook/index.md**: ハンドブックのページを追加した
- **hugo/config/blowfish/markup.toml**: iframeを読み込むための設定を追加した
- **poetry.lock**: 依存しているパッケージを更新した
- **hugo/content/about/index.md**: Aboutを移行した
- **hugo/config/blowfish/menus.ja.toml**: メニューなどの初期パラメータを設定した
- **hugo/config/_default/hugo.toml**: 設定用のディレクトリを追加した
- **docs/_static/css/heading.css**: 見出し用のCSSと分かるようにファイル名を変更した
- **docs/_static/custom.css**: h4, h5, h6のカスタムCSSを追加した
- **docs/report.md**: 開催報告記事は別ページにわけた
- **docs/conf.py**: myst_parserのextensionを追記した
- **docs/index.md**: すべてのページの見出しをカスタムCSSに合わせた形式に修正した
- **docs/_static/custom.css**: h1, h2, h3のスタイルを追加した
- **docs/conf.py**: カスタムCSSを適用した
- **themes/beautifulhugo**: Hugoのテーマを削除した
- **content/_index.md**: コンテンツ一式を削除した
- **config/_default/config.toml**: 設定ファイルを削除した
- **docs/about/index.md**: データの場所を変更した
- **docs/about/data/members.csv**: データを移動した
- **docs/about/data/**: データの場所を変更した
- **docs/conf.py**: GA4のタグを追加した
- **docs/index.md**: トップページを整理した

### Refactor

- ディレクトリ名を変更した
- 削除した
- ディレクトリ名を変更した
- ディレクトリ名を変更した

## 0.4.0 (2023-04-16)

### Added

- **docs/_templates/my-data/news-list.tmpl**: 活動報告リストのためのデータテンプレートを作成した
- **docs/about/index.md**: Aboutのインデックスを作成した
- **docs/about/index.md**: マップを追加した
- **docs/about/members.csv**: 会員数のCSVファイルを追加した
- **docs/about/members.md**: 会員数のページを追加した
- **docs/about/officials/**: 役員一覧のCSVファイルを追加した
- **docs/guidebook/handbook.md**: ハンドブックのページを追加した
- **docs/guidebook/logicmodel.md**: ロジックモデルのページを追加した
- **docs/guidebook/stempra.md**: Stempra和訳のページを追加した
- **docs/index.md**: 目次に「JACSTについて」を追加した
- **docs/index.md**: 目次に「活動報告」を追加した
- **docs/index.md**: 目次に「入会の手引き」を追加した
- **docs/index.md**: 目次に「ガイドブック」に追加した

### Fixed

- **docs/_static/jacst_prospectus.pdf**: 設立趣意書PDFを移動した
- **docs/about/index.md**: デバッグ用の表示が残っていたので削除した
- **docs/about/index.md**: 設立時の情報を追記した
- **docs/about/index.md**: 設立趣意書をDLできるようにした
- **docs/conf.py**: MyST Parserのオプションを追加した
- **docs/guidebook/handbook.md**: ハンドブックのPDFを埋め込んだ
- **docs/guidebook/logicmodel.md**: ロジックモデルのテンプレートPDFを埋め込んだ
- **docs/guidebook/stempra.md**: StempraのPDFを埋め込んだ
- **docs/post/index.md**: 2007 - 2010年度までのデータを追加した
- **docs/post/index.md**: 2011-2023年度までのデータを追加した
- **docs/post/index.md**: 個別の詳細レポートを追加した

## 0.3.0 (2023-04-15)

### Added

Sphinxでページ作成する方針に切り替えた

- **docs/_static/jacst.png**: ロゴを追加した
- **docs/conf.py**: MyST-Parserを有効にした
- **docs/conf.py**: Sphinxで初期化した
- **docs/index.md**: トップページ（仮）を作成した
- **pyproject.toml**: commitizenを有効にした
- **requirements.txt**: パッケージ一覧を追加した

### Fixed

- **.gitignore**: TeX関連のファイルを除外した
- **auto-update**: auto-update content pages: 2022-03-22 18:55:05
- **auto-update**: auto-update content pages: 2022-03-22 19:00:43
- **config/_default/config.toml**: Google Analytics (UA) を追加した
- **content/_index.md**: トップページのメッセージを修正した
- **content/page/**: 固定ページを修正した
- **content/page/about.md**: jacst.mdにファイル名を変更した
- **content/page/prospectus.md**: 設立趣意書を追加した
- **themes/beautifulhugo**: テーマを更新した

## 0.2.0 (2022-02-17)

### Fix

- **config.toml**: メニューに事務局を追加した
- **config.toml**: メニューに特設サイトを追加した
- **config.toml**: メニューに特設サイトを追加した

### Feat

- **secretariat**: 事務局メンバーを過去を含めて追加した

## 0.1.2 (2022-02-14)

### Fix

- **README.md**: タグをつける手順を追記した

## 0.1.1 (2022-02-14)

### Fix

- **themes**: テーマを更新した
- **config.toml**: slackのリンクを追加した
- **config.toml**: メニューを修正した
- **join.md**: 入会の手引きを移植した
- **themes**: もう一度サブモジュールとして追加した
- **themes**: おかしくなってたのでとりあえず削除した
- **themes**: git -> httpsに変更した
- **themes**: テーマをサブモジュールとして登録した
- **themes**: 既存のテーマを削除した
- **static**: faviconを追加した
- **favicon.ico**: Hugoのfaviconを削除した
- **run.sh**: git操作を追加した
- **reporter.py**: debagu / verbose のフラグを追加した
- **run.sh**: 作業をシェルスクリプトにまとめた
- **spreadsheet**: コンテンツを更新した
- **spreadsheet**: コンテンツを更新した
- **scripts/report**: コンテンツを更新した
- **config.toml**: delete samples from menu
- **spreadsheet**: tag列を追加した
- **config.toml**: menuからauthorsを削除した
- **config.toml**: taxonomies は不要だったので削除した
- **scripts/reporter.py**: frontmatter の tags に author を追加した
- **scripts/reporter.py**: frontmatterにauthorを追加した
- **scripts/reporter.py**: デバッグ用表示に author を追加した
- **config.toml**: taxonomiesにauthorsを追加した
- **content/post/report...md**: コンテンツを更新した
- **scripts/reporter.py**: デバッグ用の表示を追加した
- **scripts/reporter.py**: カラム名の追加に合わせて修正した
- **spreadsheet**: カラム名を変更した
- **config.toml**: logo（アバターアイコン）を非表示にした
- **config.toml**: unsplashからトップページの画像をもらってきた
- **_index.md**: トップページのメッセージを編集した
- **README.md**: README.mdを更新した
- **spreadsheet**: コンテンツの内容を更新した
- **_default/terms.html**: アイテムリストの .Title -> .LinkTitle に変更した
- **_default/single.html**: seeAlso の .Title -> .LinkTitle に変更した
- **post_prevew.html**: .Title -> .LinkTitle に変更した
- **content/post/report.md**: コンテンツを追加した
- **spreadsheet**: コンテンツを追加した
- **scripts/reporter.py**: 不要なprintを削除した
- **conent/post/report...md**: コンテンツを更新した
- **scripts/reporter.py**: add_summary を削除sした
- **scripts/reporter.py**: frontmatterを作成する部分を変更した
- **scripts/reporter.py**: メインコンテンツを書き出す内容を変更した
- **scripts/reporter.py**: more以降の文章を取得する関数を作成した
- **scripts/reporter**: リード文を取得する関数を追加した
- **scripts/reporter.py**: デバッグ用の表示内容を変更した
- **spreadsheet**: コンテンツの内容を更新した
- **spreadsheet**: 勉強レポートの内容を更新した
- **spreadsheet**: 列名を main_lead / main_more に変更した
- **conent/post/report...md**: コンテンツを更新した
- **scripts/reporter.py**: 本文を挿入した
- **spreadsheet**: dateStartを基準にソートした
- **scripts/reporter.py**: abstract を summary列 に変更した
- **spreadsheet**: summary と main の列を作ることにした
- **spreadsheet**: 勉強会のレポートを追加した
- **spreadsheet**: linkTitleをshort_titleに変更した
- **config.toml**: スプレッドシートのgidを変更した
- **content/post/report...md**: コンテンツを更新した
- **scripts/reporter.py**: 概要をメインコンテンツとして表示するようにした
- **scripts/reporter.py**: デバッグ表示用の内容を修正した
- **spreadsheet**: コンテンツの元データを整理した
- **content/post/report...md**: 活動報告を更新した
- **scripts/reporter.py**: カテゴリのf文字列を修正した
- **scripts/reporter.py**: 日付のf文字列を修正した
- **spreadsheet**: 日付のフォーマットを変更した
- **config.toml**: 日付の表示形式を変更した
- **i18n/ja.yaml**: 404ページの文章を修正した
- **i18n.ja.yaml**: 翻訳を修正した
- **config.toml**: Author に website を追加した
- **config.toml**: fixed params
- **config.toml**: title と homeTitle を別々に設定した
- **i18n/ja.yaml**: remove ja.yaml
- **config.toml**: changed theme
- **themes**: updated beautifulhugo
- **config.toml**: changed theme
- **themes**: renamed theme -> beautifuljacst
- **content/post/report...md**: 活動報告を更新した
- **reporter.py**: frontmatter用の関数を修正した
- **reporter.py**: frontmatterの作成用に関数を分割した
- **reporter.py**: デバッグ用に関数名を変更した
- **csv**: linkTitleを一部追加した
- **csv**: linkTitleを追加した
- **content/post/report...md**: 活動報告を作成した
- **reporter.py**: コンテンツ生成スクリプトのプロトタイプ
- **config.toml**: メニューを修正した
- **csv**: 列名を追加した
- **posts**: サンプルファイルを削除した
- **snapsheets**: 活動リストをDLできるようにした
- **i18n/ja.yml**: 日本語訳を修正した
- **config.toml**: hasCJKLanguageを有効にした
- **ja.yaml**: テーマからコピーした
- **content/join.md**: 入会の手引きを追加した
- **content/about.md**: 団体の紹介を追加した
- **config.toml**: タイポを修正した
- **config.toml**: ページ管理者の情報を修正した
- **config.toml**: bigimg を追加した
- **config.toml**: タイトルを変更した
- **config.toml**: 日本語に変更した
- **config.toml**: set baseurl
- **.gitignore**: ignore hugo related files
- **.gitignore**: add python related files
- **.gitignore**: ignore macOS related files
