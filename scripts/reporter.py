#! /usr/bin/env python3

from pathlib import Path

import numpy as np
import pandas as pd


def add_title(row):
    """Add title

    titleを設定する。
    short_titleがある場合は設定する。
    """

    lines = f'title = "{row.title}"\n'
    if row.short_title is np.nan:
        return lines
    lines += f'linktitle = "{row.short_title}"\n'
    return lines


def add_date(row):
    """Add date

    イベントの開催報告日を設定する
    1日開催の場合は date_start を使う
    複数日開催の場合は date_end を使う
    """

    lines = f'date = "{row.date_start}"'
    if not row.date_end is np.nan:
        lines = f'date = "{row.date_end}"'
    return lines


def add_category(row):
    """Add category"""

    lines = f'categories = ["{row.category}"]'
    return lines


def add_tags(row):
    """Add tags

    tags = ["tag1", "tag2"]
    """

    lines = f'tags = ["{row.author}"]'
    return lines


def add_author(row):
    """Add author

    記事の担当者／担当部会を設定する
    author = "名前1"
    """

    lines = f'author = "{row.author}"'
    return lines


def add_frontmatter(row):
    """Add frontmatter"""

    lines = (
        "+++",
        add_title(row),
        add_date(row),
        add_category(row),
        add_tags(row),
        add_author(row),
        "+++",
        "\n\n",
    )
    lines = ("\n").join(lines)
    return lines


def add_main_lead(row):
    """Add lead paragraph"""

    if row.main_lead is np.nan:
        return ""

    lines = ("\n", f"{row.main_lead}", "\n")
    lines = ("\n").join(lines)
    return lines


def add_main_more(row):
    """Add lead paragraph"""

    if row.main_more is np.nan:
        return ""

    lines = ("\n", f"{row.main_more}", "\n")
    lines = ("\n").join(lines)
    return lines


def add_content(row):
    """Add main content"""

    lines = (add_main_lead(row), r"<!--more-->", add_main_more(row), "\n")
    lines = ("\n").join(lines)
    return lines


def add_debug(row):
    """Add content"""

    lines = "\n---\n"
    lines += "# for DEBUG\n"
    lines += f'- date_start = "{row.date_start}"\n'
    lines += f'- date_end = "{row.date_end}"\n'
    lines += f'- category = "{row.category}"\n'
    lines += f'- title = "{row.title}"\n'
    lines += f'- linkTitle = "{row.short_title}"\n'
    lines += f'- author = "{row.author}"\n'
    lines += f'- venue = "{row.venue}"\n'
    if row.main_lead is np.nan:
        lines += f'- main_lead = "{row.main_lead}"\n'
    if row.main_more is np.nan:
        lines += f'- main_more = "{row.main_lead}"\n'
    lines += f'- url = "{row.url}"\n'
    lines += f'- remarks = "{row.remarks}"\n'
    lines += "\n\n"
    return lines


if __name__ == "__main__":
    import argparse

    # オプションを設定
    parser = argparse.ArgumentParser(description="JACSTウェブサイトのコンテンツページを生成するスクリプト")
    parser.add_argument("--debug", "-d", action="store_true")
    parser.add_argument("--verbose", "-v", action="store_true")
    args = parser.parse_args()

    fname = Path("snapd") / "_jacst_reports.csv"
    data = pd.read_csv(fname, skiprows=1)

    saved = Path("../content/post/")
    for idx, row in data.iterrows():
        savef = saved / f"report{idx+1:04d}.md"
        lines = add_frontmatter(row)
        lines += add_content(row)
        if args.debug:
            lines += add_debug(row)
        with open(savef, "w") as f:
            f.write(lines)
            if args.verbose:
                print(f"Wrote to {savef}")

    npages = len(data)
    print(f"Created {npages} pages")
