#! /usr/bin/env bash

set -e

function usage {
cat <<EOM
    Usage:
    $(basename "$0") [OPTION]...

Options:
    -h  show this help
    -l  show log (TBA)

Description:
    JACSTのウェブサイトを自動更新
    1. スプレッドシートの取得（更新がなくても）
    2. マスターデータの作成
    3. ウェブページの生成
    の一連の作業を定期的に実行する

    定期実行の方法
    1. 定期実行の際はLaunchdに登録して使う
    2. ../launchd/に移動する
    3. make install / reinstall /uninstall する
    実行内容は Makfile を確認すれば分かる

EOM
    exit 2
}

function run {
    NOW=$(date '+%Y-%m-%d %H:%M:%S')
    poetry run snapsheets
    poetry run ./reporter.py
    echo $NOW > last-synced
    MSG=$(echo "fix(auto-update): auto-update content pages": $NOW)
    echo $MSG
#    git add last-synced
    git add snapd/
    git add ../content/
    git commit -m "$MSG"
}

while getopts ":hl" OPTIONS
do
    case "$OPTIONS" in
        h) OPT_FLAG_HELP=1;;
        l) OPT_FLAG_LOG=1;;
        :) echo "[ERROR] Undefined options."; OPT_FLAG_HELP=1;;
        \?) echo "[ERROR] Undefined options."; OPT_FLAG_HELP=1;;
    esac
done


if [ -n "${OPT_FLAG_HELP}" ];then
    usage;
else
    run
fi
