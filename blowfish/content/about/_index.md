+++
title = "JACSTについて"
cardView = true
groupByYear = false
showDate = false
showWordCount = false
showReadingTime = false
showAuthor = false
+++

科学技術広報研究会（JACST：Japan Association of Communication for Science and Technology）は、研究機関や大学などの広報担当者が、所属する組織の枠をこえて、広報活動における問題意識・問題点を共有し、それらを通してお互いに助け合い、共に成長していくことを目指したネットワークです。

原則として科学技術に関する広報活動に従事する実務者が参加し、生きたネットワークの中で実務者同士が互いに支えあい向上する場とすることを目的としています。

メーリングリストや勉強会、年会などの活動を通して、参加者の知識・意識のレベルアップを図り、実務のノウハウから人材情報まで、組織横断的に広報に関する自由な情報交換を行います。

## 会員数

203名

## 機関数

136の大学・研究機関など

## 会員が所属する大学・研究機関

<iframe src="https://www.google.com/maps/d/u/0/embed?mid=1Oeq23VyLkY5TYX4-BlewOyY4SHM&ehbc=2E312F" width="100%" height="500px"></iframe>

<hr>

## カレンダー

<iframe src="https://calendar.google.com/calendar/embed?src=scsg66a0b4ntuudll0t5rrvmn4%40group.calendar.google.com&ctz=Asia%2FTokyo" style="border: 0" width="100%" height="500px" frameborder="0" scrolling="no"></iframe>

<hr>
