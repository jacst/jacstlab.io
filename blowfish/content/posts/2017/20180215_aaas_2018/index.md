+++
title = "AAAS2018にJACST参加機関で合同出展しました"
date = "2018-02-19"
tags = ["国際広報", "合同出展"]
authors = ["office"]
+++

2018年2月15日から19日に、アメリカ・テキサス州オースティンで開催されたアメリカ科学振興境界の年次大会（AAAS2018）にJACST参加機関で合同出展しました。
