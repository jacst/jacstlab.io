+++
title = "ファンダメンタルズバザールのウェブサイトを開設しました"
date = "2020-06-08"
tags = ["隣接領域", "ファンダメンタルズ"]
authors = ["fundamentalz"]
+++

隣接領域と連携した広報業務部会が「ファンダメンタルズバザール」のウェブサイトを開設しました

<!--more-->

## 関連サイト

- [ファンダメンタルズバザール](https://www.fundamentalz.jp/)
