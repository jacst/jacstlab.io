+++
title = "第23回メディア向けプレゼン会を開催しました"
date = "2024-08-30"
authors = ["media"]
cardView = true
+++

2024年8月30日（金）に第23回メディア向けプレゼン会を東京大学生産技術研究所（駒場Ⅱキャンパス）で開催しました。

<!--more-->

JACSTの国内メディアリレーション部会が主催している、「JACSTメディア向け合同プレゼン会」が、8月30日、東京大学 駒場リサーチキャンパスにある、東京大学 生産技術研究所 総合研究実験棟（An棟）大会議室にて開催されました。

今回で23回目の開催となる同会は、大学や研究機関の広報担当者が、独自の目線で所属機関のネタを紹介し、取材につなげてもらうことを目指すもので、広報担当者の修練の場ともなっています。
今回は、過去最高の19名が発表をしました。メディアからは、同会を楽しみにしている、という声も聞かれました。

大型の台風10号の影響により、現地参加を予定していた方のうち数名が急遽オンライン参加となりましたが、現地参加23名、オンライン参加10名と、合計33名のメディア関係者、会員は55名登録、37名が現地参加という盛況ぶりでした。

> ＜写真キャプション＞ 多くの参加者が集まった会場

メディアからは、テレビ番組・書籍・雑誌・ネット放送などの企画担当者、報道関係者などが集まり、それぞれの発表に熱心に聞き入りました。閉会後には懇親会も開かれ、互いの交流を深めました。

同会は全国の科学技術広報担当者とメディアの方々が直接交流できる貴重な機会で、これまでにいくつもの報道、TV番組企画、書籍化などが実現しています。次回の開催にもご期待ください！（日程未定）

> ＜写真キャプション＞プレゼン会の様子

## プレゼン会の詳細

- **開催日時**: 2024 年 8 月 30 日（金）13:00 -- 17:00
- **開催場所**: [東京大学生産技術研究所](https://www.iis.u-tokyo.ac.jp/ja/) 総合研究実験棟（An棟）大会議

### 13:00～13:30  ご挨拶・趣旨説明

- 参加メディアの自己紹介
- 東京大学生産技術研究所さま ご紹介

### 13:30～14:10  セッション I

| 番号 | タイトル | 発表者 | 所属 |
|---|---|---|---|
| 1 |『機械学習で解明する6次元空間の分子運動』 | 大林由尚 | 東京大学情報基盤センター |
| 2 |『AgeTech最前線』 | 我喜屋久 | 沖縄科学技術大学院大学（OIST） |
| 3 |『スーパーコンピュータ「富岳」の利用研究事例のご紹介』 | 内山恵津子 | 高度情報科学技術研究機構（RIST） |
| 4 |『農研機構の花研究、推し2選!』 | 小林弘佳 | 農研機構広報部 |
| 5 |『紹介します!火星衛星探査計画MMX 全搭載機器』 | 矢治健太郎 | 宇宙航空研究開発機構 |

### 14:10～14:50  セッション II

| 番号 | タイトル | 発表者 | 所属 |
|---|---|---|---|
| 6 |『生物多様性科学と社会をむすぶ 国立遺伝学研究所ABS支援室』 | 長谷川麻子 | 国立遺伝学研究所ABS支援室 |
| 7 |『そこまで来てますよ、○○社会。NIMS広報担当がコソッとお届けする最新情報』 | 鈴木康仁 | NIMS（物質・材料研究機構） |
| 8 |『ゴミが地球を救う?!都市と農村の有機物循環プロジェクト』 | 柴田幸子 | 総合地球環境学研究所 |
| 9 |『宇宙で最も正確な時計をつくる ― KEKの貢献』 | 勝田敏彦 | 高エネルギー加速器研究機構 |
| 10 |『あつまれ さんそうけんの〇〇』 | 福山康弘 | 産業技術総合研究所 |

---

休憩（20分）

---

### 15:10～15:50  セッション III

| 番号 | タイトル | 発表者 | 所属 |
|---|---|---|---|
| 11 | 『光の力で細胞膜を揺らして薬を細胞の中へ届ける―細胞膜モジュレーター』 | 渡辺真人 | 京都大学学術研究展開センター |
| 12 | 『創薬のためのヒト模倣組織・臓器を「つくる・つかむ・つなぐ」プラットフォームの開発』 | 山岸敦 | 理化学研究所 生命機能科学研究センター |
| 13 | 『海の生き物「すご技」紹介 人と海を繋ぐ北大の水産科学』 | 齋藤有香 | 北海道大学 |
| 14 | 『SEM、TEMに次ぐ、第3の電子顕微鏡』 | 餅田円 | 東京大学物性研究所 |
| 15 | 『大阪発!無邪気な大人の産業科学』 | おおたゆい | 大阪大学 産業科学研究所 |

### 15:50～16:23  セッション IV

| 番号| タイトル | 発表者 | 所属 |
|---|---|---|---|
| 16 | 『科学的リテラシーのはぐくみかた−先端科学は誰のもの?』 | 藤田弥世 | 京都大学学術研究展開センター |
| 17 | 『走行中ワイヤレス給電の最適配置が導く低炭素モビリティ・ビジョン』 | 岡田麻記子 | 東京大学生産技術研究所 |
| 18 | 『北海道が日本のワインを変える!?』 | 南波直樹 | 北海道大学 |
| 19 | 『科学者とアーティストが出会い、普遍を探る異色のプロジェクト』 | 坪井あや | 東京大学カブリ数物連携宇宙研究機構（Kavli IPMU） |

### 16:23～17:00  総合討論
