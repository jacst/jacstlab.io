+++
title = "New Scientist Live 2016にJACST参加機関で合同出展しました"
date = "2016-09-25"
tags = ["国際広報", "合同出展"]
authors = ["office"]
+++

2016年9月22日から25日にイギリス・ロンドンで開催されたNew Scientist Live 2016にJACST参加機関で合同出展しました。

<!--more-->

## 関連記事

- [ロンドンでの科学展「ニュー・サイエンティスト・ライブ」に出展しました - 国立天文台](https://www.nao.ac.jp/news/events/2016/20161013-nslive.html)

## 関連サイト

- [New Scientist Live](https://live.newscientist.com/)
- [New Scientist Live 2016](https://www.imperial.ac.uk/events/101905/new-scientist-live-2016/)
- [New Scientist Live 2016 Day1 Highlights](https://youtu.be/8vx7BMql7-Q)
