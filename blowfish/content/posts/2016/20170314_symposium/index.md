+++
title = "第3回広島大学研究力強化シンポジウムを開催しました"
date = "2017-03-14"
tags = ["シンポジウム"]
authors = ["office"]
+++

2017年3月13日、14日に、広島大学中央図書館ライブラリーホールで第3回広島大学研究協力シンポジウムを開催しました。

本シンポジウムは、広島大学が主催し、JACSTと沖縄科学技術大学院大学（OIST）が共催しました。

<!--more-->

## 関連記事

- [第3回広島大学研究力強化シンポジウムを開催しました - 広島大学](https://www.hiroshima-u.ac.jp/activities/news/38648)
