+++
title = "第3回セミナー「USサイエンスライター特別セミナー」を開催しました"
date = "2015-07-03"
tags = ["セミナー"]
authors = ["office"]
+++

2015年7月3日にサイエンスライティング研究会とJACSTの共同主催で「USサイエンスライター特別セミナー」を開催しました。

<!--more-->

ライティングはサイエンスコミュニケーションの重要な機能の1つです。
サイエンスコミュニケーションは、科学技術と社会をめぐる諸問題に対処するために登場した理念であり活動です。そのコミュニケーション手段としては、対面ではもちろん会話が中心となりますが、伝統的なマスメディアはもちろんSNSでもライティングが重要な機能を発揮します。

アメリカの第一線でサイエンスライターとして長く活躍しているリンさんに、サイエンスライティングのをめぐる動向を語っていただきました。

## 関連ページ

- [サイエンスライティング研究](https://sciencewriting.info/)
- [USサイエンスライター特別セミナー](https://sciencewriting.info/event/576/)
