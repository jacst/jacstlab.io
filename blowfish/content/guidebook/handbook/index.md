+++
title = "科学技術広報ハンドブック"
date = "2017-04-20"
summary = "JACST有志によるガイドブック班が、新任者がすぐに広報実務をはじめられるようにハンドブックを作成しました"
+++

このハンドブックは、広報新任者がすぐに広報実務を始められるようにという趣旨で、JACST有志によるガイドブック班が作成しました。

ある程度キャリアのある方にも参考にしていただけます。ぜひご活用ください。

また、本ハンドブックで紹介している事例はこちらからもご覧になれます → JACST広報事例集（リンク切れ）

<iframe src="https://drive.google.com/file/d/1RHj-0z_bpCGVSEMWKJygCDnuJQ1cvTZ5/preview" width="640" height="900" allow="autoplay"></iframe>
