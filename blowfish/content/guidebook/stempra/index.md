+++
title = "Stempra - プレスオフィサーのためのガイドブック"
date = "2011-09-27"
+++

イギリスの広報官が作っている団体、Stempra（The Science, Technology, Engineering and Medicine Public Relations）が制作したプレスオフィサーのためのガイドラインを、許諾を得て和訳しました。
<!--more-->

日本の事情と異なっている点もいくつかありますが、参考になる知見も多々あります。下からご参照ください。

<iframe src="https://drive.google.com/file/d/0B7Af0IsFy7wdT2F6NTJFU1VSRzg/preview?resourceKey=0-8iENTAu7_nqT4_z4zpl2gA" width="640" height="900" allow="autoplay"></iframe>
