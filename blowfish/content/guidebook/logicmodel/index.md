+++
title = "ロジックモデル・テンプレート"
date = "2022-01-14"
+++

科学技術広報ハンドブックの「評価とロジックモデル」（p.4〜）で紹介しているロジックモデル・テンプレートです。

<!--more-->

各自でダウンロードしてお使いください。

<iframe src="https://drive.google.com/file/d/1RCNTtYnBzPUrZvIoulNs5-W57eWXBkyw/preview" width="640" height="480" allow="autoplay"></iframe>
