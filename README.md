# 科学技術広報研究会（JACST）

科学技術広報研究会（JACST：Japan Association of Communication for Science and Technology ）は、研究機関や大学などの広報担当者が、所属する組織の枠をこえて、広報活動における問題意識・問題点を共有し、それらを通してお互いに助け合い、共に 成長していくことを目指したネットワーク団体です。

## ウェブサイト

- Google Site: https://sites.google.com/view/jacst
- GitLab Pages: https://jacst.gitlab.io/
- GitLab Repos: https://gitlab.com/jacst/jacst.gitlab.io

---

## 事前準備

- ウェブサイトのソースコードは``Git``でバージョン管理しています
- リポジトリは ``GitLab``で管理しています
- ローカルで開発を始めるときは、一番はじめに次の事前準備を実行してください

```console
// プロジェクト用のディレクトリを作成する
// （任意のディレクトリで構いません）
$ mkdir -p ~/repos/gitlab.com/jacst/
$ cd ~/repos/gitlab.com/jacst/

// プロジェクトをクローンする
$ git clone git@gitlab.com:jacst/jacst.gitlab.io.git

// プロジェクトに移動する
$ cd jacst.gitlab.io
```

## ウェブサイトをビルド

- [Hugo](https://gohugo.io/)でウェブサイトをビルドします

```console
// プロジェクトに移動する
$ cd ~/repos/gitlab.com/jacst/jacst.gitlab.io

// コンテンツ用ディレクトリ（blowfish）に移動する
$ cd blowfish

// 開発サーバーを起動する
$ hugo server -e blowfish
Start building sites …
hugo v0.136.5+extended darwin/arm64 BuildDate=2024-10-24T12:26:27Z VendorInfo=brew

                   | JA
-------------------+------
  Pages            | 108
  Paginator pages  |   3
  Non-page files   |   0
  Static files     |   7
  Processed images |   2
  Aliases          |  27
  Cleaned          |   0

Built in 104 ms
Environment: "blowfish"
Serving pages from disk
Running in Fast Render Mode. For full rebuilds on change: hugo server --disableFastRender
Web Server is available at http://localhost:1313/preview/ (bind address 127.0.0.1)
Press Ctrl+C to stop
```

- `http://localhost:1313/preview/`で開発サーバーにアクセスする
- ホットリロードに対応しているので、サイト表示を確認しながら、コンテンツを編集できる


---

（ここから下はメモ）

## 活動報告を追加

- 活動報告は、全体共有している``Googleスプレッドシート``（あとでリンクはる）に追記してください
- 個別のページは必要に応じてその都度作成します
  - Googleドキュメントで下書きを作成して、共有してください

## あとで整理する

```console
# スクリプト用のディレクトリに移動
$ cd scripts

# 活動報告を更新
$ snapsheets
$ ./reporter.py

# ルートディレクトリに移動してウェブサイトを構築
$ cd ..
$ hugo
```

# リポジトリにタグをつける

- ``commitizen``を導入してある
- ``conventional_commit``を採用している
- 開発がある程度進んだらタグをつけておく

```bash
# プロジェクトルートにいることを確認する
$ cz bump --check-consistency --changelog
bump: version 0.1.0 → 0.1.1
tag to create: 0.1.1
increment detected: PATCH

$ git push origin --tags
```

- ``cz bump --changelog``したディレクトリに``CHANGELOG.md`` が作成されるので注意する

## リポジトリの公開範囲

- ``設定`` -> ``一般`` -> ``可視性／プロジェクトの機能／権限``を展開して設定を確認
- プロジェクトの可視性 : ``公開``
- イシュー : ``アクセスできる人すべて``
- リポジトリ : ``アクセスできる人すべて``
  - マージリクエスト : ``プロジェクトメンバーのみ``
  - フォーク : ``プロジェクトメンバーのみ``
  - Git LFS : ``有効``
  - パッケージ : ``有効``
  - CI/CD : ``プロジェクトメンバーのみ``
- コンテナレジストリ : ``プロジェクトメンバーのみ``
- Analytics : ``アクセスできる人すべて``
- Requirements : ``アクセスできる人すべて``
- Security & Compliance : ``プロジェクトメンバーのみ``
- Wiki : ``無効``
- スニペット : ``無効``
- ページ : ``アクセスできる人すべて``
- Operations : ``アクセスできる人すべて``
  - メトリクスダッシュボード : ``アクセスできる人すべて``
